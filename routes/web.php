<?php

use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
		return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/user/get/{id}', 'PostController@get_user_posts')->name('user_posts');
Route::get('/post/get/{id}', 'PostController@get_post')->name('post');
Route::get('/post/create', 'PostController@create')->name('create');

Route::post('/post', 'PostController@store')->name('create_post');





