<?php

namespace App\Http\Controllers;

use App\User;
use App\Post;
use Redirect;
use Session;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller {
	/**
	 * PostController constructor.
	 */
	public function __construct() {
		$this->middleware( 'auth' );
	}

	/**
	 * Get all posts by user
	 *
	 * @param $id
	 *
	 * @return object
	 */
	public function get_user_posts( $id ) {
		$user = User::find( $id );
		if ( $user ) {
			// Found user
			$posts = $user->post;

			return view( 'posts.list', compact( 'posts' ) );

		} else {
			// User not found
			abort( 404 );
		}
	}

	/**
	 * Get posts for this user
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function get_post( $id ) {
		$post_id = Post::find( $id );

		if ( $post_id ) {
			// Found user
			$post    = $post_id;
			$authors = $post_id->author;

			return view( 'posts.single', compact( 'post', 'authors' ) );

		} else {
			// Post not found
			abort( 404 );
		}
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view( 'posts.create' );
	}

	/**
	 * Saves Post to Database
	 *
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function store( Request $request ) {

		// Validate fields
		$validator = Validator::make( $request->all(), [
			'title'        => 'required|unique:posts|max:64',
			'description'  => 'required',
			'is_published' => 'required',
		] );

		// Redirect back if validator fails
		if ( $validator->fails() ) {
			Session::flash( 'alert-warning', "Input validation failed" );

			return Redirect::back();
		}

		// Get fields
		$title        = Input::get( 'title' );
		$description  = Input::get( 'description' );
		$is_published = Input::get( 'is_published' );

		$post               = new Post;
		$post->title        = $title;
		$post->description  = $description;
		$post->is_published = $is_published;
		$post->save();

		// First save then attach to pivot table
		$user_id = Auth::id();
		$post->author()->attach( $user_id );

		Session::flash( 'alert-success', "You now have a new post" );

		return Redirect::back();
	}

}
