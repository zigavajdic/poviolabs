<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {
	/**
	 * Post fields that are fillable
	 *
	 * @var array
	 */
	protected $fillable = [
		'is_published',
		'title',
		'description',
	];

	/**
	 * A post can have many users (authors)
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function author() {
		return $this->belongsToMany(
			'App\User'
		);
	}

}
