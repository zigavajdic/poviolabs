@extends('layouts.app')

@section('content')
    <div id="app">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">List</div>

                        <div class="panel-body">
                            @if($posts->first())
                                <ul>
                                    @foreach ($posts as $post)

                                        <li><a href="/post/get/{{$post->id}}">{{ $post->title }}</a></li>

                                    @endforeach
                                </ul>
                            @else
                                This user has no posts.
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <router-view></router-view>
    </div>
@endsection
