@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Post</div>

                    <div class="panel-body">
                        <div class="meta-authors">
                            @foreach($authors as $author)
                                <span class="badge badge-primary">{{$author->name}}</span>
                            @endforeach
                        </div>
                        <h2>{{$post->title}}</h2>
                        <p>{{$post->description}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
